module.exports = function (grunt) {
  grunt.initConfig({
    connect: {
      server: {
        options: {
          port: 8080,
          base: './'
        }
      }
    },
    copy: {
      main: {
        cwd: 'src',
        src: ['**/*', '!**/less/**'],
        dest: 'app',
        expand: true
      },
    },
    less: {
      development: {
        options: {
          paths: ["assets/css"]
        },
        files: { "app/css/main.css": "src/less/main.less" }
      },
      watch: {
        files: "*.less",
        tasks: ["less"]
      }
    },
    watch: {
      styles: {
        options: {
          livereload: true,
          spawn: false,
          event: ['added', 'deleted', 'changed']
        },
        files: ['**/*.less'],
        tasks: ['less']
      },
      scripts: {
        options: {
          livereload: true,
          event: ['added', 'deleted', 'changed']
        },
        files: ['src/js/*.js'],
        tasks: ['copy:main', 'uglify:build']
      }
    },
    uglify: {
      options: {
        banner: "/*! Minified files */\n"
      },
      build: {
        src: ["src/js/product-listing.js", "src/js/form.js"],
        dest: "app/js/main.min.js"
      }

    }

  });
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('default', ['connect:server', 'less:development', 'uglify:build', 'copy:main', 'watch']);
};
