/* Product Listing scripts */
$(function () {
  $.ajax({
    url: "./data/products.json"
  }).done(function (result) {
    var template = $('#products').html();
    var output = Handlebars.compile(template);
    var renderThis = output(result)
    $('#product-listing').html(renderThis);
  });

  /* Toggle favorite */
  $(document).on('click', '.product-favourite span', function () {
    $(this).toggleClass('isFavourite')
  });

  /* Toggle view */
  $(document).on('click', '.product-view-toggle button', function () {
    $(this).siblings('button').removeClass('active');
    $(this).addClass('active');
    $(this).closest('#product-listing').removeAttr('class').addClass($(this).data('view'));
  })

  Handlebars.registerHelper('ifCond', function (v1, v2, options) {
    if (v1) {
      var res = "";
      for (var i = 1; i <= v1; i++) {
        res += "<span></span>";
      }
      return res;
    } else {
      return options.inverse(this);
    }
  });

});