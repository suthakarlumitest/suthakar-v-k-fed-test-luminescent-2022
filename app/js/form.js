/* Form validation scripts */
class FormValidator {
  constructor(form, fields) {
    this.form = form
    this.fields = fields
  }

  initialize() {
    this.validateOnEntry()
    this.validateOnSubmit()
  }

  validateOnSubmit() {
    let self = this
    this.form.addEventListener('submit', e => {
      e.preventDefault()
      self.fields.forEach(field => {
        const input = document.querySelector(`#${field}`)
        self.validateFields(input)
      })
    })
  }

  validateOnEntry() {
    let self = this
    this.fields.forEach(field => {
      const input = document.querySelector(`#${field}`)

      input.addEventListener('input', event => {
        self.validateFields(input)
      })
    })
  }

  validateFields(field) {

    // Empty check
    if (field.value.trim() === "") {
      this.setStatus(field, `${field.previousElementSibling.innerText} cannot be blank`, "error")
    } else {
      /* Name & Surname validation */
      if (field.name === "name" || field.name === "surname") {
        var reg_name = /^[a-zA-Z\s]*$/;
        if (reg_name.test(field.value)) { //
          this.setStatus(field, null, "success");
        } else {
          this.setStatus(field, "Please enter only characters", "error")
        }
      }

      // Email validation
      if (field.type === "email") {
        const re = /\S+@\S+\.\S+/
        if (re.test(field.value)) {
          this.setStatus(field, null, "success")
        } else {
          this.setStatus(field, "Please enter valid email address", "error")
        }
      }

      // Phone validation
      if (field.name === "phone") {
        const reg_phone = /^[+]{0,1}[\s0-9]*$/;
        if (reg_phone.test(field.value)) { //
          this.setStatus(field, null, "success");
        } else {
          this.setStatus(field, "Please enter valid phone number", "error")
        }
      }

      // Password validation
      if (field.type === "password") {
        const reg_password = /^(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$/;;
        if (reg_password.test(field.value)) { //
          this.setStatus(field, null, "success");
        } else {
          this.setStatus(field, "Password should be at least 8 digit and contain one special character", "error")
        }
      }
    }
  }

  setStatus(field, message, status) {
    const errorMessage = field.parentElement.querySelector('.error-message')

    if (status === "success") {
      if (errorMessage) { errorMessage.innerText = "" }
      field.classList.remove('input-error')
    }

    if (status === "error") {
      field.parentElement.querySelector('.error-message').innerText = message
      field.classList.add('input-error')
    }
  }
}

const form = document.querySelector('#form')
const fields = ["name", "surname", "email", "password", "phone"]

const validator = new FormValidator(form, fields)
validator.initialize()